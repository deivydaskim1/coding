<?php

 $servername = "localhost";
 $username = "root";
 $password = "";
 $dbname = "restoran";
 
 $conn = new mysqli($servername, $username, $password, $dbname);
 if ($conn->connect_error) {
     die("Connection failed: " . $conn->connect_error);
 }
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Date</title>
    <style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    </style>
</head>
<body>
<h1>Reservation</h1>
<table>
    <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Persons</th>
        <th>Date</th>
    </tr>

 <?php
 $sql = "SELECT id, firstname, lastname, persons, datee FROM reservation ORDER BY datee DESC";
 $result = $conn->query($sql);
 
 if ($result->num_rows > 0) {
     while($row = $result->fetch_assoc()) {
         echo "<tr><td>".$row["firstname"]."</td><td>".$row["lastname"]."</td><td>".$row["persons"]."</td><td>".$row["datee"]."</td></tr>";
    }
 } else {
     echo "Error";
 }
 ?>
 </table>
 <h1>Messages</h1>
 <table>
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Topic</th>
        <th>Message</th>
    </tr>
 <?php
 $sql = "SELECT id, username, email, topic, textmessage FROM contact";
 $result = $conn->query($sql);
 
 if ($result->num_rows > 0) {
     while($row = $result->fetch_assoc()) {
         echo "<tr><td>".$row["username"]."</td><td>".$row["email"]."</td><td>".$row["topic"]."</td><td>".$row["textmessage"]."</td></tr>";
     }
 } else {
     echo "Error";
 }
 $conn->close();
 ?>
 </table>
</body>
</html>