<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Bahiana|Oswald" rel="stylesheet">
    <link rel="shortcut icon" href="images/favicon.ico" />
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/slide.css">    
    <script
    src="https://code.jquery.com/jquery-3.2.1.js"
    integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>
    <title>Dine Restaurant</title>
</head>
<body>
    <header>
        <nav id ="navbar">
            <div class="wrapper">
                <a href="#"><img src="./images/logo-white.png" alt=""></a>
                <a id ="icon" class="icon" href="javascript:void(0);" onclick="myFunction()">&#9776;</a>
                <ul>
                    <li><a id="home" href="#">HOME</a></li>
                    <li><a id="about" href="#">ABOUT</a></li>
                    <li><a id="menu" href="#">MENU</a></li>
                    <li><a id="galery" href="#">GALERY</a></li>
                    <li><a id="reservation" href="#">RESERVATION</a></li>
                    <li><a id="contact" href="#">CONTACT</a></li>
                </ul>
            </div>
        </nav>
        <div class="ms-header">
            <div class="ms-slider">
                <ul class="ms-slider__words">
                    <li class="ms-slider__word">ALWAYS READY</li>
                    <li class="ms-slider__word">LOVED BY PEOPLE</li>
                    <li class="ms-slider__word">DINE WITH US</li>
                    <!-- This last word needs to duplicate the first one to ensure a smooth infinite animation -->
                    <li class="ms-slider__word">ALWAYS READY</li>
                </ul>
            </div>
            <h6>143TH AVENUE, LONDON</h6>
            <h6>TEL: 1 (234) 5555</h6>
            <div class="star">
                <div class="star_line"></div>
                <span>&#x2605</span>
                <div class="star_line"></div>
            </div>
            <button>BOOK A TABLE</button>
        </div>
    </header>
    <main>
        <section class="wrapper about">
            <div class="info">
                <h1>ABOUT US</h1>
                <p>Corper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio</p>
            </div>
            <div class="slider">
                <img src="./images/about.jpg" alt="">
            </div>
        </section>
        <section class="wrapper menu">
            <h1>MENU</h1>
            <h2>Cidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ullamcorper suscipit lobortis nisl ut aliquip ex ea co nisl ut aliquip ex ea co mmodo consequat.</h2>
            <div class="star">
                <div class="star_line"></div>
                <span>&#x2605</span>
                <div class="star_line"></div>
            </div>
            <div class="menu-cont">
                <div class="menu-item">
                    <h1>DESSERTS</h1>
                    <div class="food">
                        <h3>CALMARI FRA DIAVOLO</h2>
                        <span>$11.95</span>
                    </div>
                    <p>Our Fried Calamari Sauteed with Hot & Sweet Vinegar Peppers in Our Marinara Sauce</p>
                    <div class="food">
                        <h3>CALMARI FRA DIAVOLO</h2>
                        <span>$11.95</span>
                    </div>
                    <p>Our Fried Calamari Sauteed with Hot & Sweet Vinegar Peppers in Our Marinara Sauce</p>
                    <div class="food">
                        <h3>CALMARI FRA DIAVOLO</h2>
                        <span>$11.95</span>
                    </div>
                    <p>Our Fried Calamari Sauteed with Hot & Sweet Vinegar Peppers in Our Marinara Sauce</p>
                    <div class="food">
                        <h3>CALMARI FRA DIAVOLO</h2>
                        <span>$11.95</span>
                    </div>
                    <p>Our Fried Calamari Sauteed with Hot & Sweet Vinegar Peppers in Our Marinara Sauce</p>
                    <div class="food">
                        <h3>CALMARI FRA DIAVOLO</h2>
                        <span>$11.95</span>
                    </div>
                    <p>Our Fried Calamari Sauteed with Hot & Sweet Vinegar Peppers in Our Marinara Sauce</p>
                    <div class="food">
                        <h3>CALMARI FRA DIAVOLO</h2>
                        <span>$11.95</span>
                    </div>
                    <p>Our Fried Calamari Sauteed with Hot & Sweet Vinegar Peppers in Our Marinara Sauce</p>
                </div>
                <div class="menu-item">
                    <h1>LUNCH</h1>
                    <div class="food">
                        <h3>OLD FASHIONED RICE BALL</h2>
                        <span>$6.95</span>
                    </div>
                    <p>Homemade Rice Balls Made with an Italian Cheese Blend then Lightly Breaded and Fried. Topped with Mozzarella and Marinara</p>
                                    <div class="food">
                        <h3>OLD FASHIONED RICE BALL</h2>
                        <span>$6.95</span>
                    </div>
                    <p>Homemade Rice Balls Made with an Italian Cheese Blend then Lightly Breaded and Fried. Topped with Mozzarella and Marinara</p>
                                        <div class="food">
                        <h3>OLD FASHIONED RICE BALL</h2>
                        <span>$6.95</span>
                    </div>
                    <p>Homemade Rice Balls Made with an Italian Cheese Blend then Lightly Breaded and Fried. Topped with Mozzarella and Marinara</p>
                                        <div class="food">
                        <h3>OLD FASHIONED RICE BALL</h2>
                        <span>$6.95</span>
                    </div>
                    <p>Homemade Rice Balls Made with an Italian Cheese Blend then Lightly Breaded and Fried. Topped with Mozzarella and Marinara</p>
                                        <div class="food">
                        <h3>OLD FASHIONED RICE BALL</h2>
                        <span>$6.95</span>
                    </div>
                    <p>Homemade Rice Balls Made with an Italian Cheese Blend then Lightly Breaded and Fried. Topped with Mozzarella and Marinara</p>
                                        <div class="food">
                        <h3>OLD FASHIONED RICE BALL</h2>
                        <span>$6.95</span>
                    </div>
                    <p>Homemade Rice Balls Made with an Italian Cheese Blend then Lightly Breaded and Fried. Topped with Mozzarella and Marinara</p>
                </div>
                <div class="menu-item">
                    <h1>DINNER</h1>
                    <div class="food">
                        <h3>STEAK STROGANOFF</h2>
                        <span>$12.5</span>
                    </div>
                    <p>Served in a Creamy Gorgonzola Sauce with Crostini Toast</p>
                    <div class="food">
                        <h3>STEAK STROGANOFF</h2>
                        <span>$12.5</span>
                    </div>
                    <p>Served in a Creamy Gorgonzola Sauce with Crostini Toast</p>
                    <div class="food">
                        <h3>STEAK STROGANOFF</h2>
                        <span>$12.5</span>
                    </div>
                    <p>Served in a Creamy Gorgonzola Sauce with Crostini Toast</p>
                    <div class="food">
                        <h3>STEAK STROGANOFF</h2>
                        <span>$12.5</span>
                    </div>
                    <p>Served in a Creamy Gorgonzola Sauce with Crostini Toast</p>
                    <div class="food">
                        <h3>STEAK STROGANOFF</h2>
                        <span>$12.5</span>
                    </div>
                    <p>Served in a Creamy Gorgonzola Sauce with Crostini Toast</p>
                    <div class="food">
                        <h3>STEAK STROGANOFF</h2>
                        <span>$12.5</span>
                    </div>
                    <p>Served in a Creamy Gorgonzola Sauce with Crostini Toast</p>
                </div>
            </div>
            <button class="book">BOOK NOW ></button>
            <div class="star">
                <div class="star_line"></div>
                <span>&#x2605</span>
                <div class="star_line"></div>
            </div>
        </section>
        <section class="wrapper gallery">
            <h1>GALLERY</h1>
            <h2>SHOWCASE OF OUR RESTAURANT</h2>
            <!-- Container for the image gallery -->
        <div class="container">
          <!-- Full-width images with number text -->
          <div class="mySlides">
            <div class="numbertext">1 / 6</div>
              <img src="./images/slider1.jpg" style="width:100%">
          </div>
        
          <div class="mySlides">
            <div class="numbertext">2 / 6</div>
              <img src="./images/slider2.jpg" style="width:100%">
          </div>
        
          <div class="mySlides">
            <div class="numbertext">3 / 6</div>
              <img src="./images/slider3.jpg" style="width:100%">
          </div>
        
          <div class="mySlides">
            <div class="numbertext">4 / 6</div>
              <img src="./images/slider4.jpg" style="width:100%">
          </div>
        
          <div class="mySlides">
            <div class="numbertext">5 / 6</div>
              <img src="./images/slider5.jpg" style="width:100%">
          </div>
        
          <div class="mySlides">
            <div class="numbertext">6 / 6</div>
              <img src="./images/slider6.jpg" style="width:100%">
          </div>
        
          <!-- Next and previous buttons -->
          <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
          <a class="next" onclick="plusSlides(1)">&#10095;</a>
        
          <!-- Image text -->
          <div class="caption-container">
            <p id="caption"></p>
          </div>
        </div>
        </section>
        <section class="reservation">
            <h1>RESERVATION</h1>
            <h2>WE SERVE ALL HOLIDAYS</h2>
            <div class="star">
                <div class="star_line"></div>
                <span>&#x2605</span>
                <div class="star_line"></div>
            </div>
            <div class="form-cont">
                <form action="reservation.php" method="POST">
                    <input type="text" name="firstname" id="firstname" placeholder="Firstname" required="required">
                    <input type="text" name="lastname" id="lastname" placeholder="Lastname" required="required">
                    <select name="persons" id="persons">
                        <option value="1">1 person</option>
                        <option value="2">2 persons</option>
                        <option value="3">3 persons</option>
                        <option value="4">4 persons</option>
                        <option value="5">5 persons</option>
                        <option value="6">6 persons</option>
                    </select>
                    <!-- <input type="date" name="date" id="date"> -->
                    <input id="date" type='date' name="date" max='2018-02-12' required="required"></input>
                    <input type="submit" name="submit" value="SUBMIT">
                </form>
            </div>
        </section>
        <section class="wrapper contact">
            <h1>CONTACT</h1>
            <h2>FIND US ON MAP</h2>
            <div class="content">
                <div id = "map" class="item">
                    
                </div>
                <div class="item">
                    <form action="contact.php" method="POST" target="_blank">
                        <input type="text" name="name" id="name" placeholder="YOUR NAME">
                        <input type="email" name="email" id="email" placeholder="YOUR EMAIL">
                        <input type="text" name="subject" id="subject" placeholder="YOUR SUBJECT">
                        <textarea name="message" id="message" cols="30" rows="10" placeholder="YOUR MESSAGE"></textarea>
                        <input type="submit" name="submit2" value="SEND">
                    </form>
                </div>
            </div>
        </section>
    </main>
    <footer>
        <div class="wrapper">
            <div class="item">
                <h1>ADDRESS</h1>
                <p>15 Beaufain Street<br>Charleston, SC 29401</p>
            </div>
            <div class="item">
                <h1>FOLLOW US</h1>
                <div class="items">
                    <a href="#"><img src="./images/facebook.png" alt=""></a>
                    <a href="#"><img src="./images/twitter.png" alt=""></a>
                    <a href="#"><img src="./images/google.png" alt=""></a>
                    <a href="#"><img src="./images/youtube.png" alt=""></a>
                    <a href="#"><img src="./images/instagram.png" alt=""></a>
                    <a href="#"><img src="./images/tumblr.png" alt=""></a>
                </div>
                <p>© 2017 Made by Deivydas</p>
            </div>
            <div class="item">
                <h1>CONTACT US</h1>
                <p>info@domain.com<br>843-212-0920</p>
            </div>
        </div>
    </footer>
    <script src="app.js"></script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBG8lkArDKRyGND0SDrOihVueSxYi-EYeM&callback=initMap">
    </script>
</body>
</html>