// $(document).ready(function(){
//     $('input[type="submit"]').click(function(){
//         var kiekis = $("#skaicius").val();
//         var suma = kiekis * 69.75;
//         $("#convert").text(kiekis + "EUR = " + suma + "RUB")
//     })
// })



function celciusToFahrenheit(celcius){
    var fahrenheit = celcius * 9/5 + 32;
    return fahrenheit;
}

function celciusToKelvin(celcius){
    var kelvin = (parseInt(celcius) + 273.15);
    return kelvin;
}

$('#celcius').on('keyup', function(){
    var inputVal =$('#celcius').val();
    var toFahrenheit = celciusToFahrenheit(inputVal);
    var toKelvin = celciusToKelvin(inputVal);
    var fahInput =$('#fahrenheit').val(toFahrenheit);
    var kelvinInput =$('#kelvin').val(toKelvin);
 });