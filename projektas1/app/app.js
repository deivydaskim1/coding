// position of Lithuania center
const klaipeda = {lat: 55.6876556, lng: 21.1633865};

var position =  {lat: 55.1694, lng: 23.8813};


var locations = [
  ['Big Kebabai', 55.6791884, 21.1866939, 4],
  ['Kingo kebabai', 55.7001447, 21.1682567, 5],
  ['Sultono kebabai', 55.6723782, 21.1702015, 3],
];

var infowindow = new google.maps.InfoWindow();
var marker, i;


function initAutocomplete() {
    //Map init
    var map = new google.maps.Map(document.getElementById('map'), {
      center: klaipeda,
      zoom: 12,
      mapTypeId: 'roadmap'
    });

    // Markers ir click
    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
      });
    
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow = new google.maps.InfoWindow({
            content: locations[i][0]
        });
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
    google.maps.event.addListener(map, 'mousedown', function() {
      infowindow.close();
    });
  
    // Link search box to my input
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
  
    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });
  
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();
  
      if (places.length == 0) {
        return;
      }
  
      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };
  
        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });
}


function navbaropen(){
    $("nav").css("display","block")
}
function navbarclose(){
    $("nav").css("display","none")
}
