// var gulp = require('gulp');
// // Requires the gulp-sass plugin
// var sass = require('gulp-sass');
// var browserSync = require('browser-sync').create();


// gulp.task('sass', function() {
//   return gulp.src('app/scss/**/*.scss') // Gets all files ending with .scss in app/scss
//     .pipe(sass())
//     .pipe(gulp.dest('app/css'))
//     .pipe(browserSync.reload({
//       stream: true
//     }))
// });

// gulp.task('browserSync', function() {
//     browserSync.init({
//       server: {
//         baseDir: 'app'
//       },
//     })
//   })

// gulp.task('watch', ['browserSync', 'sass'], function (){
//   gulp.watch('app/scss/**/*.scss', ['sass']); 
//   // Reloads the browser whenever HTML or JS files change
//   gulp.watch('app/*.html', browserSync.reload); 
//   gulp.watch('app/*.php', browserSync.reload); 
//   gulp.watch('app/js/**/*.js', browserSync.reload); 
// });

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload,
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch');

gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "http://http://localhost/coding_bitbucket/baigiamasis/",
    });
});

gulp.task('sass', function () {
  gulp.src('sass/**/*.scss')
    .pipe(sass({
        outputStyle:'compressed',
    })
    .on('error', sass.logError))
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
     }))
    .pipe(gulp.dest('./css'));
});


gulp.task('watch', function() {
    // Watch .scss files
    gulp.watch('sass/*.scss', ['sass']).on("change", browserSync.reload);
    gulp.watch('sass/**/*.scss', ['sass']).on("change", browserSync.reload);
    gulp.watch('js/*.js', ['js']).on("change", browserSync.reload);
    gulp.watch('index.php').on("change", browserSync.reload);
});


var jsInput = {
    js: 'js/dev/**/*.js'
}

var jsOutput = 'js/dist/';


gulp.task('js', function(){
  return gulp.src(jsInput.js)
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./js/dist/'))
});

gulp.task('default',['sass', 'browser-sync','watch', 'js']);