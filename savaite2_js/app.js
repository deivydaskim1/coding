// var player = {
//     runSpeed : 20,
//     firstName: "Vytas",
//     lastName: "Bėgikas",
//     height: 1.85,
//     age: 23,
//     fullName : function() {
//         return this.firstName + " " + this.lastName;
//     }
// }

function createPlayer(speed, name, surname, height, age) {
    this.runSpeed = speed;
    this.firstName = name;
    this.lastName = surname;
    this.height = height;
    this.age = age;
    this.fullName = function() {
        return this.firstName + " " + this.lastName;
    }
}

var player1 = new createPlayer(20,"Vytas","Vėjas",1.85,23)
var player2 = new createPlayer(24,"Jonas","Linze",1.92,21)
var player3 = new createPlayer(24,"Dovydas","Biras",1.79,22)
var players = [];
var maxSpeed = 0;
var fastestMan = "";

players.push(player1, player2, player3);

players.forEach(function(element,i){
    if (players[i].runSpeed === maxSpeed){
        fastestMan += ", " + players[i].fullName();
    }
    if (players[i].runSpeed > maxSpeed){
        maxSpeed = players[i].runSpeed;
        fastestMan = players[i].fullName();
    }
})

var test = document.getElementById("test")
test.innerHTML = player1.fullName()+", "+player2.fullName()+", "+player3.fullName()+" Greičiausias:"+fastestMan+" kurio greitis: "+ maxSpeed;